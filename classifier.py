import torch
import torch.utils.data
import torchvision
import torchvision.transforms as transforms
import matplotlib.pyplot as plt

from models import LeNet, load_model

def test_classifier(model, device, dataroot='fsgm/images', image_size=28, batch_size=1, show=False):
    test_loader = torch.utils.data.DataLoader(
        torchvision.datasets.ImageFolder(root=dataroot,transform=torchvision.transforms.Compose([
            torchvision.transforms.Resize(image_size),
            torchvision.transforms.ToTensor()
            ])),
        batch_size=batch_size
    )
    predictions=[]
    print('-----> Testing <-----')
    with torch.no_grad():
        correct = 0
        total = 0
        for i, data in enumerate(test_loader, 0):
            image = data[0].to(device)
            image = image[:,0,:,:].unsqueeze(0)
            print(image.shape)
            out = model(image)
            print(out.max(1, keepdim=True)[1])

            if show:
                img = image.view(image_size,image_size).cpu().detach().numpy()
                plt.imshow(img, cmap='gray')
                plt.show()            

            _, predicted = torch.max(out.data, 1)
            #correct += (predicted == label).sum().item()
            #total += label.size(0)
            predictions.append(predicted.cpu().detach().numpy())
            print('Predictions ==> {}'.format(predictions))
            print('Score ==> {}\n'.format(out.max(1)))
            #print('Iter: {} - Accuracy: {}'.format(i, correct / total))


device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
model = LeNet(in_channels=1).to(device)
optimizer = torch.optim.Adam(model.parameters(), lr=0.001)
model,_,_ = load_model(model, optimizer, root="fsgm/saved_models/lenet_mnist.pth")
#train_ae(model, dataroot="~/AI/Datasets/cifar10/", result_dir="./result/recons")
test_classifier(model, device, dataroot='fsgm/images', image_size=28, batch_size=1, show=True)