import torch
import torchvision
import torch.utils.data
from torch.autograd import Variable
import torch.nn.functional as F
import matplotlib.pyplot as plt

from models import LeNet, load_model

###############
# Parameters
###############
images_dir = './images'
model_dir = 'fsgm/saved_models/lenet_mnist.pth'
image_size = 28
epsilon = 0.3
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
lr = 0.001

###############
# Dataset
###############
test_loader = torch.utils.data.DataLoader(
     torchvision.datasets.MNIST('./mnist/', train=False, download=True, transform=torchvision.transforms.Compose([
        torchvision.transforms.Resize(image_size),
        torchvision.transforms.ToTensor(),
        ])),
    batch_size=1, shuffle=True)

#test_loader = torch.utils.data.DataLoader(
#    torchvision.datasets.ImageFolder(root=images_dir ,transform=torchvision.transforms.Compose([
#        torchvision.transforms.Resize(image_size),
#        torchvision.transforms.ToTensor()
#        ])),
#    batch_size=1
#)

###############
# Models
###############
classifier = LeNet(in_channels=1).to(device)
optimizer = torch.optim.Adam(classifier.parameters(), lr=lr)
classifier,_,_ = load_model(classifier, optimizer=optimizer, root=model_dir)

##############
# FSGM
##############
def fgsm_attack(image, epsilon, data_grad):
    sign_grad = data_grad.sign()
    perturbed_image = image + epsilon * sign_grad
    perturbed_image = torch.clamp(perturbed_image, 0, 1)
    return perturbed_image

def test( model, device, test_loader, epsilon ):
    correct = 0
    for i, data in enumerate(test_loader, 0):
        data = data[0].to(device)
        # Make image grayscale
        data = data[:,0,:,:].unsqueeze(0)
        data.requires_grad = True

        output = model(data)
        init_pred = output.max(1, keepdim=True)[1]

        loss = F.nll_loss(output, init_pred.squeeze().unsqueeze(0))
        model.zero_grad()
        loss.backward()

        data_grad = data.grad.data
        perturbed_data = fgsm_attack(data, epsilon, data_grad)
        output = model(perturbed_data)

        final_pred = output.max(1, keepdim=True)[1]
        if final_pred.item() != init_pred.item():
            torchvision.utils.save_image(data.data, filename='fsgm/results/{}_real_{}_{}.png'.format(i,init_pred.item() ,final_pred.item()))
            torchvision.utils.save_image(perturbed_data.data, filename='fsgm/results/{}_fgsm_{}_{}.png'.format(i,init_pred.item() ,final_pred.item()))
        else:
            correct += 1

    final_acc = correct/float(len(test_loader))
    print("Epsilon: {}\tTest Accuracy = {} / {} = {}".format(epsilon, correct, len(test_loader), final_acc))

    # Return the accuracy and an adversarial example
    return final_acc

accuracies = []
examples = []
classifier.eval()

acc = test(classifier ,device, test_loader, epsilon)
accuracies.append(acc)



